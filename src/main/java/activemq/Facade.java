package activemq;

import esper.EsperTest;
import gui.FrameOPC;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.swing.SwingUtilities;
import marshalling.DemoAleksMarshalling;
import marshalling.DemoMarshalling;
import model.ERPData;
import model.OPCDataItem;
import stateless4j.TestMachine;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Marcelo Aleksandravicius <marcelo.aleks -at- gmail.com>
 */
public class Facade {

    //Cria a �nica inst�ncia do Facade (singleton)
    private static Facade INSTANCE = new Facade();

    private FrameOPC frame = null;

    //descritor do thread
    private Thread threadStart = null;

    private ObservableList<ERPData> dataERP = FXCollections.observableArrayList();

    //##########################################################################
    //##########################################################################
    public ObservableList<ERPData> getDataERP() {
        return dataERP;
    }

    
    /**
     * Helper method to aid in the saving of OPC messages just arrived
     *
     * @return ERPData
     */
    public ERPData getLastERPArrived() {
        if (dataERP.size() > 0)
            return dataERP.get(dataERP.size() - 1);
        else
            return null;
    }

    /**
     * Set the Object data to the ERP table
     *
     * @param dataERP
     */
    public void setDataERP(ObservableList<ERPData> dataERP) {
        this.dataERP = dataERP;
    }
    //##########################################################################

    //##########################################################################
    private Facade() {

    }

    public FrameOPC getFrame() {
        return frame;
    }

    public void setFrame(FrameOPC frame) {
        this.frame = frame;
    }

    public void startIndustryProcess() {

        threadStart = new Thread(
            () -> {
                //JAXB Marshalling / Unmarshalling 
                DemoMarshalling demo2 = new DemoMarshalling();
                DemoAleksMarshalling demo = new DemoAleksMarshalling();
                demo.run();

                //Message Queue Connection using JMS 
                QueueConnection q = new QueueConnection();

                //Message Queue Connection using JMS via Apache Camel 
                QueueConnectionUsingCamel qc = new QueueConnectionUsingCamel();
                qc.run();

                // State Machine 
                TestMachine t = new TestMachine();
                t.run();

                //Esper test 
                EsperTest esperTest = new EsperTest();
                esperTest.run();
            });

        threadStart.start();

    }

    public void setPhotocellTime(String txt) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                frame.setPhotocellTime(txt);
            }

        });

    }

    public static synchronized Facade getInstance() {

        return INSTANCE;
    }

}
