package stateless4j;

public enum PartStates {

	INIT,
	DRILLING,
	MILLING,
        HEATING,
        MILLING_SPEED,
        MILLING_HEAT,
	FINISHED,
        DRILLING_SPEED,
        DRILLING_HEAT,
      
        
}
