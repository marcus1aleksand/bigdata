

import activemq.Facade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import esper.EsperTest;
import activemq.QueueConnection;
import activemq.QueueConnectionUsingCamel;
import gui.FrameOPC;
import marshalling.DemoAleksMarshalling;
import marshalling.DemoMarshalling;
import stateless4j.TestMachine;


/**
 * Just comment out the code pieces you want.
 * @author julian
 *
 */
public class Main {

	Logger _log = 	LogManager.getLogger(Main.class);
	
	public static void main(String[] args) {
		
                FrameOPC frame = new FrameOPC();
                frame.setVisible(true);
                Facade.getInstance().setFrame(frame);
            
                
		//JAXB Marshalling / Unmarshalling 
		DemoMarshalling demo2 = new DemoMarshalling(); 
		DemoAleksMarshalling demo = new DemoAleksMarshalling(); 
		demo.run();
		
		//Message Queue Connection using JMS 
		QueueConnection q = new QueueConnection();
		
		//Message Queue Connection using JMS via Apache Camel 
		QueueConnectionUsingCamel qc = new QueueConnectionUsingCamel(); 
		qc.run();
		
		// State Machine 
		TestMachine t = new TestMachine(); 
		t.run(); 
	
		//Esper test 
		EsperTest esperTest = new EsperTest(); 
		esperTest.run();
	}
}
