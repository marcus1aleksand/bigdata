/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.fx.controller;

import activemq.Facade;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import model.ERPData;
import model.OPCDataItem;

/**
 * FXML Controller class
 *
 * @author Marcelo Aleksandravicius <marcelo.aleks -at- gmail.com>
 */
public class FXMLController implements Initializable {

    @FXML
    private TableView tableERP;

    @FXML
    private TableView tableOPC;

    @FXML
    private Label labProcessError;

    @FXML
    private Label labelTotalERP;

    @FXML
    private Button pbStart;

    //Colunas
    @FXML
    private TableColumn ERP_order;
    @FXML
    private TableColumn ERP_customer;
    @FXML
    private TableColumn ERP_material;
    @FXML
    private TableColumn ERP_timestamp;

    //OPC Data item
    @FXML
    private TableColumn OPC_name;
    @FXML
    private TableColumn OPC_status;
    @FXML
    private TableColumn OPC_value;
    @FXML
    private TableColumn OPC_timestamp;
    
    
    @FXML
    private TableColumn<?, ?> ERP_overallstatus;

    @FXML
    public void onStartIndustry(ActionEvent ev) {
        System.err.println("start !!");
        Facade.getInstance().startIndustryProcess();

        labProcessError.setText("Running...");
        pbStart.setDisable(true);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        //ERP Table Clolumns configure
        ERP_order.setCellValueFactory(
            new PropertyValueFactory<ERPData, String>("OrderNumber"));

        ERP_customer.setCellValueFactory(
            new PropertyValueFactory<ERPData, String>("CustomerNumber"));

        ERP_material.setCellValueFactory(
            new PropertyValueFactory<ERPData, String>("MaterialNumber"));

        ERP_timestamp.setCellValueFactory(
            new PropertyValueFactory<ERPData, String>("TimeStamp"));

        tableERP.setItems(Facade.getInstance().getDataERP());

        //OPC Table Clolumns configure
        OPC_name.setCellValueFactory(
            new PropertyValueFactory<OPCDataItem, String>("itemName"));

        OPC_status.setCellValueFactory(
            new PropertyValueFactory<OPCDataItem, String>("status"));

        OPC_value.setCellValueFactory(
            new PropertyValueFactory<OPCDataItem, String>("value"));

        OPC_timestamp.setCellValueFactory(
            new PropertyValueFactory<OPCDataItem, String>("timestamp"));

        //Inserts the tableERP row selection listener
        tableERP.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection == null)
                tableERP.getSelectionModel().clearSelection();
            else {
                ERPData erpData = (ERPData) tableERP.getSelectionModel().getSelectedItem(); 
                tableOPC.setItems( erpData.getDataOPC() );
                
            }
                
            
        });

    }

}
