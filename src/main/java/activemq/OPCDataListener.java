package activemq;

import java.io.StringReader;
import java.util.logging.Level;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import model.ERPData;

import model.OPCDataItem;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The listener class takes new messages and unmarshalls them to Java Objects.
 *
 * @author julian
 *
 */
public class OPCDataListener implements MessageListener {

    private Logger _log = LogManager.getLogger(OPCDataListener.class);

    private JAXBContext _ctx;

    private Unmarshaller _unmarshaller;

    /**
     * Default Constructor
     */
    public OPCDataListener() {
        try {
            _ctx = JAXBContext.newInstance(OPCDataItem.class);
            _unmarshaller = _ctx.createUnmarshaller();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        _log.debug("New data listener created.");
    }

    @Override
    public void onMessage(Message arg0) {
        _log.debug("New OPC message arrived!");

        TextMessage tmpMessage = null;
        OPCDataItem opcObj;
        ERPData   lastERP = null;

        if (arg0 instanceof TextMessage) {
            StringReader is = null;

            try {
                tmpMessage = (TextMessage) arg0;
                is = new StringReader(tmpMessage.getText());
                opcObj = (OPCDataItem) _unmarshaller.unmarshal(is);
                
                lastERP = Facade.getInstance().getLastERPArrived();
                if (lastERP != null) {
                    lastERP.addOPCItem(opcObj);
                    
                }
            
            
                    
                System.err.println("==> " + opcObj.getItemName());

                String value = "nada";
                try {

                    value = (String) opcObj.getValue().toString();
                    System.err.println("==> " + value);
                } catch (Exception ex) {
                    System.err.println("SEM presen�a de campo no xml value");
                }
                if (opcObj.getItemName().equals("Lichtschranke 1")) {
//                    Long tempo = Long.valueOf( (String)opcObj.getValue() );

//                    Facade.getInstance().setPhotocellTime(value);
//                    Facade.getInstance().insertMessage(opcObj);
                }

            } catch (JMSException ex) {
                java.util.logging.Logger.getLogger(OPCDataListener.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JAXBException ex) {
                java.util.logging.Logger.getLogger(OPCDataListener.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                is.close();
            }

        } else {
            _log.warn("Unknown format, marshalling aborted.");
            return;
        }

        try {
            _log.debug(tmpMessage.getText());
        } catch (JMSException e) {
            e.printStackTrace();
        }

        //Do something with the erp data! 
    }
}
