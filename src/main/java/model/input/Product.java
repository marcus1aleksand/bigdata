package model.input;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @Expose
    private Double em1;
    @Expose
    private Double em2;
    @Expose
    private Double a1;
    @Expose
    private Double a2;
    @Expose
    private Double b2;
    @Expose
    private Double b1;
    @Expose
    private String overallStatus;
    @SerializedName("ts_start")
    @Expose
    private Integer tsStart;
    @SerializedName("ts_stop")
    @Expose
    private Integer tsStop;

    /**
     *
     * @return The em1
     */
    public Double getEm1() {
        return em1;
    }

    /**
     *
     * @param em1 The em1
     */
    public void setEm1(Double em1) {
        this.em1 = em1;
    }

    /**
     *
     * @return The em2
     */
    public Double getEm2() {
        return em2;
    }

    /**
     *
     * @param em2 The em2
     */
    public void setEm2(Double em2) {
        this.em2 = em2;
    }

    /**
     *
     * @return The a1
     */
    public Double getA1() {
        return a1;
    }

    /**
     *
     * @param a1 The a1
     */
    public void setA1(Double a1) {
        this.a1 = a1;
    }

    /**
     *
     * @return The a2
     */
    public Double getA2() {
        return a2;
    }

    /**
     *
     * @param a2 The a2
     */
    public void setA2(Double a2) {
        this.a2 = a2;
    }

    /**
     *
     * @return The b2
     */
    public Double getB2() {
        return b2;
    }

    /**
     *
     * @param b2 The b2
     */
    public void setB2(Double b2) {
        this.b2 = b2;
    }

    /**
     *
     * @return The b1
     */
    public Double getB1() {
        return b1;
    }

    /**
     *
     * @param b1 The b1
     */
    public void setB1(Double b1) {
        this.b1 = b1;
    }

    /**
     *
     * @return The overallStatus
     */
    public String getOverallStatus() {
        return overallStatus;
    }

    /**
     *
     * @param overallStatus The overallStatus
     */
    public void setOverallStatus(String overallStatus) {
        this.overallStatus = overallStatus;
    }

    /**
     *
     * @return The tsStart
     */
    public Integer getTsStart() {
        return tsStart;
    }

    /**
     *
     * @param tsStart The ts_start
     */
    public void setTsStart(Integer tsStart) {
        this.tsStart = tsStart;
    }

    /**
     *
     * @return The tsStop
     */
    public Integer getTsStop() {
        return tsStop;
    }

    /**
     *
     * @param tsStop The ts_stop
     */
    public void setTsStop(Integer tsStop) {
        this.tsStop = tsStop;
    }

}
