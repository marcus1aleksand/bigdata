package stateless4j;

public enum Triggers {

	MILLING_ON,
	MILLING_SPEED,
        MILLING_HEAT,
        HEATING_ON,
        HEATING_OFF,
	MILLING_OFF,
	DRILLING_ON,
        DRILLING_HEAT,
	DRILLING_OFF,
	DRILLING_SPEED,
}
